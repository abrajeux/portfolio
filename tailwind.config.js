/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      width: {
        'content': 'fit-content',
      },
      height: {
        'content': 'fit-content',
      },
    },
  },
  plugins: [],
};

