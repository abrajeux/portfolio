import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {useRef} from 'react';

const Slot = ({options, roll, index, direction, speed}) => {
  const slotRef = useRef(null);
  const startedRef = useRef(false);
  const [intervalId, setIntervalId] = useState(0);
  const [timeoutId, setTimeoutId] = useState(0);

  // Start idle once on mounting and clean interval and timeout on unmount
  useEffect(() => {
    if (startedRef.current) {
      return;
    } else {
      startedRef.current = true;
      // idleRoll();
    }

    return () => {
      clearInterval(intervalId);
      clearTimeout(timeoutId);
    };
  }, []);

  // Start and stop rolling based on given prop
  useEffect(() => {
    if (roll) {
      clearInterval(intervalId);
      setIntervalId(0);
      startedRef.current = false;
      setTimeoutId(setTimeout(() => startRoll(), Math.random() * 800 + 500));
    } else {
      if (intervalId !== 0) stopRoll();
    }
  }, [roll]);

  const startRoll = () => {
    const slot = slotRef.current;
    setIntervalId(setInterval(() => {
      if (direction === 'down') {
        if (slot.scrollTop - speed <= 0) {
          slot.scrollTop = slot.scrollHeight;
        } else slot.scrollTop -= speed;
      } else {
        if (slot.scrollTop + speed + slot.clientHeight >= slot.scrollHeight) {
          slot.scrollTop = 0;
        } else slot.scrollTop += speed;
      }
    }, 1));
  };

  const stopRoll = () => {
    console.log('Stopping roll');
    if (intervalId !== 0) {
      const slot = slotRef.current;
      const stopPosition = (slot.clientHeight * index) + (slot.clientHeight / 2);
      const stopInterval = setInterval(() => {
        if ((stopPosition > (slot.scrollTop + slot.clientHeight/4)) && (stopPosition < slot.scrollTop + (slot.clientHeight * 3 /4))) {
          clearInterval(intervalId);
          setIntervalId(0);
          slot.scrollTo({
            top: stopPosition + (slot.clientHeight / 2),
            behavior: 'smooth',
          });
          // setTimeoutId(setTimeout(() => {idleRoll();}, 5000));
          clearInterval(stopInterval);
        }
      });
    }
  };

  /* const idleRoll = () => {
    const slot = slotRef.current;
    setIntervalId(setInterval(() => {
      if (direction === 'down') {
        if (slot.scrollTop - speed <= 0) {
          slot.scrollTop = slot.scrollHeight;
        } else slot.scrollTop -= speed;
      } else {
        if (slot.scrollTop + speed + slot.clientHeight >= slot.scrollHeight) {
          slot.scrollTop = 0;
        } else slot.scrollTop += speed;
      }
    }, 20));
  };*/


  return (
    <div className={'h-24 w-16 relative rounded-md border border-slate-500 overflow-hidden'}>
      <div className={'absolute w-full h-full bg-gradient-to-b from-slate-400/40 via-transparent to-slate-400/40'}/>
      <div
        className={'h-24 w-16 px-2 overflow-hidden bg-white'}
        ref={slotRef}
      >
        {options.map((option, index) => (
          <div key={index} className={'h-24 flex items-center justify-center'}>
            <div>
              <img src={option.icon} alt={option.name} />
            </div>
          </div>
        ))}
        <div className={'h-24 flex items-center justify-center'}>
          <div>
            <img src={options[0].icon} alt={options[0].name} />
          </div>
        </div>
      </div>
    </div>
  );
};

Slot.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  roll: PropTypes.bool.isRequired,
  duration: PropTypes.number,
  index: PropTypes.number,
  direction: PropTypes.oneOf(['up', 'down']),
  speed: PropTypes.number,
};

Slot.defaultProps = {
  duration: 1000,
  index: 0,
  direction: 'down',
  speed: 1,
};

export default Slot;
