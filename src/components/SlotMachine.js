import React, {useState} from 'react';
import Slot from './Slot';
// import PropTypes from 'prop-types';

const technologies = [
  {name: 'React', icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png'},
  {name: 'Vue', icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png'},
  {name: 'Angular', icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png'},
  {name: 'Svelte', icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Svelte_Logo.svg/1200px-Svelte_Logo.svg.png'},
];

const rollDuration = 5000;
const slots = 3;

const SlotMachine = () => {
  const [roll, setRoll] = useState(false);
  const [resultIndex, setResultIndex] = useState(Math.floor(Math.random() * technologies.length));

  const launchRoll = () => {
    if (!roll) {
      setResultIndex(Math.floor(Math.random() * technologies.length));
      setRoll(true);
      setTimeout(() => {
        setRoll(false);
      }, rollDuration);
    }
  };

  return (
    <div className={'bg-red-200 w-content p-8 rounded-md space-y-4 flex flex-col justify-center'}>
      <div className={'flex space-x-2 w-full'}>
        {[...Array(slots)].map((slot, index) => (
          <Slot
            key={index}
            options={technologies}
            roll={roll}
            index={resultIndex}
            direction={index % 2 === 0 ? 'down' : 'up'}
          />
        ))}
      </div>
      <button
        onClick={launchRoll}
        disabled={roll}
        className={`py-4 px-8 rounded-xl ${roll ? 'bg-slate-200' : 'bg-red-500 hover:bg-red-700'}`}
      >
        <p className={'uppercase font-roboto font-black text-white'}>Roll !</p>
      </button>
    </div>
  );
};

SlotMachine.propTypes = {

};


export default SlotMachine;
