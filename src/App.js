import React from 'react';
import SlotMachine from './components/SlotMachine';

const App = () => {
  return (
    <div className="App">
      <SlotMachine />
    </div>
  );
};

export default App;
